# kinit
Validate credentials using the Kerberos `kinit` executable. For PHP.

**Warning!** This script relies on the internal implementation of the `kinit` executable and as such should be used with extreme care (use at your own risk).

## Installation
### Via Command Line
```sh
$ composer config repositories.geiger-it/kinit git https://bitbucket.org/geiger-it/kinit.git
```
```sh
$ composer require geiger-it/kinit
```
### Via composer.json
```json
    "require": {
        "geiger-it/kinit": "^1.0"
    },
    "repositories": {
        "geiger-it/kinit": {
            "type": "git",
            "url": "https://bitbucket.org/geiger-it/kinit.git"
        }
    }
```